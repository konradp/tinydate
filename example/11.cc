#include <iostream>
#include <tinydate/tinydate.hpp>
using std::cout;
using std::endl;
using namespace tinydate;

int main()
{
    // Month decrement, year change
    cout << "Test 10: decrementing by month" << endl;
    date j("2016-01-31");
    for(int i = 0; i < 5; i++) {
        cout << j.get_prev_month(i).GetString() << endl;
    }
    cout << endl;
}

