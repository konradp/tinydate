#include <iostream>
#include <tinydate/tinydate.hpp>
using std::cout;
using std::endl;
using namespace tinydate;

int main()
{
    // Month incrementing
    cout << "Test 5: Incrementing by one month" << endl;
    date d("2015-10-27");
    for(int i = 0; i < 10; i++) {
    cout << d.GetString() << endl;
    d.next_month();
    }
    cout << endl;
}

