#include <iostream>
#include <tinydate/tinydate.hpp>
using std::cout;
using std::endl;
using namespace tinydate;

int main()
{
    // Check date validity
    cout << "Test 2: Check date validity" << endl;
    for(auto i : {
        "1800-12-01",
        "1700-13-01",
        "1800-02-30"
    }) {
    cout << "is_valid(\"" << i << "\"): "
        << (date(i).is_valid()? "valid":"NOT valid") << endl;
    }
    cout << endl;
}

