#include <iostream>
#include <tinydate/tinydate.hpp>
using std::cout;
using std::endl;
using namespace tinydate;

int main()
{
    // Check if leap years
    cout << "Test 1: Check if specified years are leap years" << endl;
    date d("1970-02-02");
    for(auto i : {"2000", "1600", "1700", "1800", "1900"}) {
    d.set_year(i);
    cout << "is_leap(" << i << "): "
        << (d.is_leap()? "true":"false") << endl;
    }
    cout << endl;
}

