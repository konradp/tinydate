#include <iostream>
#include <tinydate/tinydate.hpp>
using std::cout;
using std::endl;
using namespace tinydate;

int main()
{
    // Day decrement, year change
    cout << "Test 8: decrementing by one day" << endl;
    date h("2017-01-02");
    for(int i = 0; i < 10; i++) {
    cout << h.GetString() << endl;
    h.prev_day();
    }
    cout << endl;
}

