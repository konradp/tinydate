#include <iostream>
#include <tinydate/tinydate.hpp>
using std::cout;
using std::endl;
using namespace tinydate;

int main()
{
    // Day increment, year change
    cout << "Test 7: Incrementing by one day" << endl;
    date g("2016-12-27");
    for(int i = 0; i < 10; i++) {
    cout << g.GetString() << endl;
    g.next_day();
    }
    cout << endl;
}

