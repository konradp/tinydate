#include <ctime>
#include <iostream>
#include <tinydate/tinydate.hpp>
using namespace tinydate;
using std::cout;
using std::endl;

int main()
{
    // time_t time(): returns NOW
    // ctime:  time_t -> string
    // mktime: tm -> time_t
    // gmtime/localtime: time_t -> tm?

    // Get Now timestamp, reset time to 00:00:00
    time_t t_now = time(0);
    cout << "Now time_t: " << t_now << endl;

    cout << "Convert timestamp to tm struct" << endl;
    struct tm tm_now;
    struct tm* tm_tmp;
    tm_tmp = localtime(&t_now);
    tm_now = *tm_tmp;
    cout << "Reset hours to 00:00:00" << endl;
    // Reset hours to 00:00:00
    tm_now.tm_hour = 0;
    tm_now.tm_min = 0;
    tm_now.tm_sec = 0;
    t_now = mktime(&tm_now);
    cout << "Now time_t: " << t_now << endl;
    cout << "DATE: "
         << 1900 + tm_now.tm_year << "-"
         << tm_now.tm_mon << "-"
         << tm_now.tm_mday << endl;
    

    // From tm struct to timestamp
    struct tm t;
    time_t time_then;
    //t = getdate("18-03-31");
    t = {
        0,          // sec
        0,          // min
        0,          // hour
        20,         // mday
        3 - 1,      // mon
        2018 - 1900 // year
                    // wday
                    // yday
                    // isdst
    };

    cout << "date: " << endl
        << t.tm_sec << endl
        << t.tm_min << endl
        << t.tm_hour << endl
        << t.tm_mday << endl
        << t.tm_mon << endl
        << t.tm_year << endl
        << t.tm_wday << endl
        << t.tm_yday << endl
        << t.tm_isdst << endl;
    time_then = mktime(&t);
    cout << "Then: " << (long) time_then << endl;

}

