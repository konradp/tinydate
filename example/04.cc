#include <iostream>
#include <tinydate/tinydate.hpp>
using std::cout;
using std::endl;
using namespace tinydate;

int main()
{
    // Check comparison operators
    cout << "Test 3: Check date comparison" << endl;
    date a("2015-05-04");
    for(auto i : {
        "2015-05-03",
        "2015-05-04",
        "2015-05-05",
        "2015-04-04",
        "2015-05-04",
        "2015-06-04",
        "2014-05-03",
        "2015-05-03",
        "2016-05-03"}) {
    date b(i);
    	//cout << i << " < " << a.GetString() << "? " << (b < a)? "true" : "false" << endl;
    cout << i << " < " << a.GetString() << "? "
        << ((b < a)? "true" : "false") << endl;
    }
    cout << endl;
}

