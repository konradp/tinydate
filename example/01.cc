#include "common.hpp"
#include <iostream>
#include <tinydate/tinydate.hpp>
using std::cout;
using std::endl;
using namespace tinydate;

int main()
{
    // Check default date
    date d;
    cout << "Default date(today): " << d.GetString() << endl;
    cout << "Timestamp: " << d.GetTimestamp() << endl;
    d.prev_day();
    cout << "Prev day timestamp: " << d.GetTimestamp() << endl;
}

