#include <algorithm>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <array>

// https://stackoverflow.com/questions/478898/how-to-execute-a-command-and-get-output-of-command-within-c-using-posix

std::string shell(const char* cmd) {
    std::array<char, 128> buffer;
    std::string str;
    std::shared_ptr<FILE> pipe(popen(cmd, "r"), pclose);
    if(!pipe) throw std::runtime_error("popen() failed!");
    while(!feof(pipe.get())) {
        if(fgets(buffer.data(), 128, pipe.get()) != nullptr)
            str += buffer.data();
    }

    str.erase(
        std::remove(
            str.begin(),
            str.end(),
            '\n'),
        str.end()
    );

    return str;
}

