[![pipeline status](https://gitlab.com/konradp/tinydate/badges/master/pipeline.svg)](https://gitlab.com/konradp/tinydate/commits/master)
[![coverage report](https://gitlab.com/konradp/tinydate/badges/master/coverage.svg)](https://gitlab.com/konradp/tinydate/commits/master)

# Tinydate library
A small date library for use in my own projects.

## Usage
In your project, `include` the library header

    #include <tinydate.hpp>

Create a `tinydate::date` instance.

    tinydate::date d("2018-02-04");

Then, use the class methods to enjoy the library's handy date manipulation methods.

    std::cout << d.GetString();
    std::cout << d.GetTimestamp();
    std::cout << d.is_leap();
    std::cout << d.prev_day();

## Examples
Compile and run the examples

    cd example/
    make
    ./01

## Tests
Compile and run tests

    cd test/
    make
    make test
    make coverage

