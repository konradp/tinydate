#include "common.hpp"
#include <cassert>
#include <iostream>
#include <tinydate/tinydate.hpp>
using std::cout;
using std::endl;
using namespace tinydate;

int main()
{
    // Specific date timestamp
    date d("2010-04-01");

    cout <<
    "Assert: " <<
        d.GetTimestamp() <<
        " == " <<
        shell("date -d \"2010-04-01\" +%s") << endl;
    assert(
        d.GetTimestamp()
        ==
        std::stoi(shell("date -d \"2010-04-01\" +%s"))
    );
}

