#include <iostream>
#include <tinydate/tinydate.hpp>
using std::cout;
using std::endl;
using namespace tinydate;

int main()
{
    // Day decrement, year change
    cout << "Test 9: decrementing by one day" << endl;
    date h1("2016-06-21");
    for(int i = 0; i < 10; i++) {
    cout << h1.GetString() << endl;
    h1.prev_day();
    }
    cout << endl;
}

