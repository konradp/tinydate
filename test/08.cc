#include <iostream>
#include <tinydate/tinydate.hpp>
using std::cout;
using std::endl;
using namespace tinydate;

int main()
{
    // Is_set method
    cout << "Test 6: Is_set() method" << endl;
    date e("2005-01-02");
    date f;
    cout << "Created date with `date my_date(\"2005-01-02\")`" << endl
    << "my_date.is_set(): " << (e.is_set()? "true" : "false") << endl;
    cout << "Created date with `date my_date()`" << endl
    << "my_date.is_set(): " << (f.is_set()? "true" : "false") << endl;
    cout << endl;
}

