#include "common.hpp"
#include <cassert>
#include <iostream>
#include <tinydate/tinydate.hpp>
using std::cout;
using std::endl;
using namespace tinydate;

int main()
{
    // Now timestamp
    date d;
    cout <<
    "Assert: " <<
        d.GetRightNowTimestamp() <<
        " == " <<
        shell("date +%s") << endl;
    assert(
        d.GetRightNowTimestamp()
        ==
        std::stoi(shell("date +%s"))
    );
}

