#include <cassert>
#include <iostream>
#include <tinydate/tinydate.hpp>
using std::cout;
using std::endl;
using namespace tinydate;

int main()
{
    cout << "**Date validity" << endl;
    cout << "Assert: d(\"1800-12-01\").is_valid() == true" << endl;
    assert( date("1800-12-01").is_valid() == true );

    cout << "Assert: d(\"1700-13-01\").is_valid() == false" << endl;
    assert( date("1700-13-01").is_valid() == false );

    cout << "Assert: d(\"1800-02-30\").is_valid() == false" << endl;
    assert( date("1800-02-30").is_valid() == false );
}

