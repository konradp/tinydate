#include <cassert>
#include <iostream>
#include <tinydate/tinydate.hpp>
using std::cout;
using std::endl;
using namespace tinydate;

int main()
{
    // Check comparison operators
    cout << "**Date comparison" << endl;
    date a("2015-05-04");
    for(auto i : {
            std::make_pair("2015-05-03", true),
            { "2015-05-04", false },
            { "2015-05-05", false },
            { "2015-04-04", true },
            { "2015-05-04", false },
            { "2015-06-04", false },
            { "2014-05-03", true },
            { "2015-05-03", true },
            { "2016-05-03", false }
    }) {
        date b(i.first);
        cout << "Assert: " << i.first << " < " << a.GetString()
             << " == " << i.second << endl;
        assert( (b < a) == i.second );
    }
}

