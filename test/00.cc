#include "common.hpp"
#include <cassert>
#include <iostream>
#include <tinydate/tinydate.hpp>
using std::cout;
using std::endl;
using namespace tinydate;

int main()
{
    // Default date string
    cout << "**Default date" << endl;
    date d;
    cout <<
    "Assert: " <<
        d.GetString() <<
        " == " <<
        shell("date +%Y-%m-%d") << endl;
    assert(
        d.GetString()
        ==
        shell("date +%Y-%m-%d")
    );
}

