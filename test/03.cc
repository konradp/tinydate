#include <cassert>
#include <iostream>
#include <tinydate/tinydate.hpp>
using std::cout;
using std::endl;
using namespace tinydate;

int main()
{
    cout << "**Leap years" << endl;
    date d;

    d.set_year("2000");
    cout << "Assert: " << d.is_leap() << " == true" << endl;
    assert(d.is_leap() == true);

    d.set_year("1600");
    cout << "Assert: " << d.is_leap() << " == true" << endl;
    assert(d.is_leap() == true);

    d.set_year("1700");
    cout << "Assert: " << d.is_leap() << " == false" << endl;
    assert(d.is_leap() == false);

    d.set_year("1800");
    cout << "Assert: " << d.is_leap() << " == false" << endl;
    assert(d.is_leap() == false);

    d.set_year("1900");
    cout << "Assert: " << d.is_leap() << " == false" << endl;
    assert(d.is_leap() == false);
}

