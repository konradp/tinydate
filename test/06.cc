#include <iostream>
#include <tinydate/tinydate.hpp>
using std::cout;
using std::endl;
using namespace tinydate;

int main()
{
    // Day incrementing
    cout << "Test 4: Incrementing by one day" << endl;
    date d("2015-04-27");
    for(int i = 0; i < 10; i++) {
        cout << d.GetString() << endl;
        d.next_day();
    }
    cout << endl;
}

